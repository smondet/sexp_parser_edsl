#! /usr/bin/env ocaml
 open Printf
 module List = ListLabels
 let say fmt = ksprintf (printf "Please-> %s\n%!") fmt
 let cmdf fmt =
   ksprintf (fun s -> ignore (Sys.command s)) fmt
 let chain l =
   List.iter l ~f:(fun cmd ->
       printf "! %s\n%!" cmd;
       if  Sys.command cmd = 0
       then ()
       else ksprintf failwith "%S failed." cmd
     )
 let args = Array.to_list Sys.argv
 let in_build_directory f =
   cmdf "mkdir -p _build/";
   Sys.chdir "_build/";
   begin try
     f ();
   with
     e ->
     Sys.chdir "../";
     raise e
   end;
   Sys.chdir "../";
   ()


let build () =
  in_build_directory (fun () ->
      chain [
        "pwd";
        "cp ../sexp_parser_edsl.ml .";
        "ocamlfind ocamlc -package sexplib,sexplib.syntax -syntax camlp4o -c sexp_parser_edsl.ml -o sexp_parser_edsl.cmo";
        "ocamlfind ocamlopt -package sexplib,sexplib.syntax -syntax camlp4o -c sexp_parser_edsl.ml  -annot -bin-annot -o sexp_parser_edsl.cmx";
        "ocamlc sexp_parser_edsl.cmo -a -o sexp_parser_edsl.cma";
        "ocamlopt sexp_parser_edsl.cmx -a -o sexp_parser_edsl.cmxa";
        "ocamlopt sexp_parser_edsl.cmxa sexp_parser_edsl.a -shared -o sexp_parser_edsl.cmxs";
      ];
    )


let install () =
    in_build_directory (fun () ->
        chain [
          "ocamlfind install sexp_parser_edsl ../META sexp_parser_edsl.cmx sexp_parser_edsl.cmo sexp_parser_edsl.cma sexp_parser_edsl.cmi sexp_parser_edsl.cmxa sexp_parser_edsl.cmxs sexp_parser_edsl.a sexp_parser_edsl.o"
        ])


let uninstall () =
    chain [
      "ocamlfind remove sexp_parser_edsl"
    ]


let merlinize () =
    chain [
      "echo 'S .' > .merlin";
      "echo 'B _build' >> .merlin";
      "echo 'PKG sexplib' >> .merlin";
"echo 'PKG sexplib.syntax' >> .merlin";
     ]


let build_doc () =
    in_build_directory (fun () ->
        chain [
          "mkdir -p doc";
                       sprintf "ocamlfind ocamldoc -package sexplib,sexplib.syntax -syntax camlp4o -charset UTF-8 -keep-code -colorize-code -html sexp_parser_edsl.ml -d doc/";
        ])


let name = "sexp_parser_edsl"

let () = begin
match args with
| _ :: "build" :: [] ->(
say "Building";
build ();
say "Done."
)
| _ :: "build_doc" :: [] ->(
say "Building Documentation";
build_doc ();
say "Done."
)
| _ :: "install" :: [] ->(
say "Installing";
install ();
say "Done."
)
| _ :: "uninstall" :: [] ->(
say "Uninstalling";
uninstall ();
say "Done."
)
| _ :: "merlinize" :: [] ->(
say "Updating `.merlin` file";
merlinize ();
say "Done."
)
| _ :: "clean" :: [] ->(
say "Cleaning";
cmdf "rm -fr _build";
say "Done."
)
| _ ->(
say "usage: ocaml %s [build|install|uninstall|clean|build_doc|melinize]" Sys.argv.(0)
)

end

