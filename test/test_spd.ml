#! /bin/sh

PACKAGES=core,sexplib.syntax

MD5=`md5sum $0  | cut -d ' ' -f 1`
BASE=/tmp/ocaml_script_$MD5/
mkdir -p $BASE

ML_FILE=${BASE}/source.ml
EXEC=${BASE}/`basename $0`

if test -f $BASE
then
  $EXEC $*
  RETURN_CODE=$?
else

  SKIP=`awk '/^__OCAML_FOLLOWS__/ { print NR + 1; exit 0; }' $0`
  echo "#$SKIP \"$0\"" > $ML_FILE
  tail -n +$SKIP $0 >> $ML_FILE

  ocamlfind ocamlopt  -I _build/ sexp_parser_edsl.cmxa -thread -package $PACKAGES \
   -syntax camlp4o -linkpkg -o $EXEC $ML_FILE \
    && $EXEC $*
  RETURN_CODE=$?
fi
exit $RETURN_CODE

__OCAML_FOLLOWS__
(**************************************************************************)
(*  Copyright (c) 2013,      Sebastien Mondet <seb@mondet.org>,           *)
(*                                                                        *)
(*  Permission to use, copy, modify, and/or distribute this software for  *)
(*  any purpose with or without fee is hereby granted, provided that the  *)
(*  above  copyright notice  and this  permission notice  appear  in all  *)
(*  copies.                                                               *)
(*                                                                        *)
(*  THE  SOFTWARE IS  PROVIDED  "AS  IS" AND  THE  AUTHOR DISCLAIMS  ALL  *)
(*  WARRANTIES  WITH  REGARD  TO  THIS SOFTWARE  INCLUDING  ALL  IMPLIED  *)
(*  WARRANTIES  OF MERCHANTABILITY AND  FITNESS. IN  NO EVENT  SHALL THE  *)
(*  AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL  *)
(*  DAMAGES OR ANY  DAMAGES WHATSOEVER RESULTING FROM LOSS  OF USE, DATA  *)
(*  OR PROFITS,  WHETHER IN AN  ACTION OF CONTRACT, NEGLIGENCE  OR OTHER  *)
(*  TORTIOUS ACTION,  ARISING OUT  OF OR IN  CONNECTION WITH THE  USE OR  *)
(*  PERFORMANCE OF THIS SOFTWARE.                                         *)
(**************************************************************************)

open Core.Std

let say fmt =
  ksprintf (fun s -> eprintf "%s\n%!" s) fmt

let return_code = ref 0
let should_not_return_zero () = return_code := 5

let fail_test msg =
  should_not_return_zero ();
  say ">> TEST FAILED: [%s]" msg
let test_assert msg cond =
  if not cond then (fail_test msg) else ()
let fail_testf msg = ksprintf fail_test msg

let test_assertf cond fmt =
  ksprintf (fun s -> test_assert s cond) fmt

let return o = `Ok o
let fail e = `Error e

let test_1 () =
  let open Sexp_parser_edsl in
  let module AST = struct
    type expr = [
      | `int of int
      | `var of string
      | `tuple of expr * expr
    ] with sexp
    type dsl = [
      | `let_binding of string * expr
      | `expr of expr
    ] list with sexp
    let syntax_error loc e = `syntax_error (loc, e)
    let dsl_toplevel_name = "DSL Top-level statements"
    let dsl_grammar =
      let open Meta_parser in
      let identifier =
        apply string (fun s ~loc ->
            if String.for_all s Char.is_alphanum
            then return s else fail (syntax_error loc (`not_an_identifier s)))
      in
      let rec expr_grammar =
        lazy (Lazy.force (
            try_in_order ~name:"DSL Expression" [
              apply (tagged ~tag:"tuple"
                       (tuple (expr_grammar) (expr_grammar)))
                ~f:(fun (a, b) ~loc -> return (`tuple (a, b)));
              apply integer ~f:(fun i ~loc -> return (`int i));
              apply identifier ~f:(fun s ~loc -> return (`var s));
            ]
          )) in
      apply ~f:(fun statements ~loc -> return (statements: dsl))
        (tagged ~tag:"dsl"
           (sequence
              (try_in_order ~name:dsl_toplevel_name [
                  apply  ~f:(fun (id, expr) ~loc -> return (`let_binding (id, expr)))
                    (tagged ~tag:"let" (tuple identifier (expr_grammar)));
                  apply ~f:(fun e ~loc -> return (`expr e)) (expr_grammar);
                ])))
  end in
  let open AST in
  let parse_string sexp =
    (Sexp.Annotated.of_string (String.strip sexp)
     |> Meta_parser.parse ~syntax_error dsl_grammar) in
  let test sexp expected =
    match parse_string sexp with
    | o when expected o -> ()
    | `Ok other ->
      fail_testf "Parsing\n    %S\n\ngave:\n    %S"
        sexp (sexp_of_dsl other |> Sexp.to_string_hum)
    | `Error e ->
      fail_testf "Parsing:\n    %S\nerror:\n    %s"
        sexp (<:sexp_of<
                [> `syntax_error of
                Meta_parser.location * [> Meta_parser.syntax_error | `not_an_identifier of string]
                ]
              >>  e |> Sexp.to_string_hum);
  in
  let test_ok s e = test s ((=) (`Ok e)) in
  let test_ko s e = test s ((=) (`Error e)) in
  test_ok "(dsl x)" [`expr (`var "x")];
  test_ok "
    (dsl
     (let (x )(42)) ;; some comment
     (let y ((tuple \"string\" 3)))
     (tuple x (tuple y 42))
     x)
     " [
    `let_binding ("x", `int 42);
    `let_binding ("y", `tuple (`var "string", `int 3));
    `expr (`tuple (`var "x", `tuple (`var "y", `int 42)));
    `expr (`var "x");
  ];
  test_ko "()" (syntax_error
                  (Meta_parser.location ~from_position:(1, 0) ~to_position:(1,1) ())
                  (`no_matching_rule "()"));
  test_ko "(dsl)"
    (syntax_error
       (Meta_parser.location ~from_position:(1, 1) ~to_position:(1,4) ())
       (`no_matching_rule "dsl"));
  test "(dsl (let x))" (function
    | `Error (`syntax_error (some_location,
                             `nothing_left_to_try (name_opt, msg))) ->
      name_opt = Some dsl_toplevel_name
      && some_location.Meta_parser.start = (1,5)
      && some_location.Meta_parser.stop = (1,11)
    | _ -> false);
  ()

let () =
  test_1 ();
  say "Test: Done.";
  exit !return_code
