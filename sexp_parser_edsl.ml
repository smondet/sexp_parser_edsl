

type ('a, 'b) result = [
  | `Ok of 'a
  | `Error of 'b
]
let return x : (_, _) result = `Ok x
let fail x : (_, _) result = `Error x
let (>>=) x f : (_, _) result =
  match x with
  | `Ok o -> f o
  | `Error e -> `Error e
let (>><) x f : (_, _) result = f x

(**

The module type [META_PARSER] describes the API.

*)
module type META_PARSER = sig

(**
The type [t] is partially exposed to make OCaml understand that it is
a [Lazy.t].
*)
  type ('dsl, 'error) grammar
  type ('dsl, 'error) t = ('dsl, 'error) grammar lazy_t

  type location = { start: int * int; stop: int * int } with sexp
  val location:
    ?from_line:int -> ?from_character:int -> ?from_position:(int * int) ->
    ?to_line:int -> ?to_character:int -> ?to_position:(int * int) -> unit ->
    location

(**
{3 Basic Constructors}
 *)

  val keyword : string -> (unit, 'error) t
  val left_and_continue :
    ?number:int ->
    ('a, 'b) t -> ('c, 'b) t -> ('a * 'c, 'b) t
  val sequence: ('a, 'b) t -> ('a list, 'b) t
  val try_in_order: ?name:string -> ('a, 'b) t list -> ('a, 'b) t

  val integer : (int, 'a) t
  val string :  (string, 'a) t
  val float :  (float, 'a) t
  val apply :
    ('a, 'error) t -> f:('a -> loc:location -> ('c, 'error) result) -> ('c, 'error) t

(**
{3 High-level Constructors }
 *)

  val tagged : ('a, 'b) t -> tag:string -> ('a, 'b) t
  val tuple:
    ('a, 'b) t -> ('c, 'b) t -> ('a * 'c, 'b) t

(**
The errors “added” by the `parse` functions (the user will add it's
own and they will be merged).
*)
  type syntax_error =
    [ `not_a_float of string
    | `incomplete_left_side of string * string
    | `not_an_integer of string
    | `no_matching_rule of string
    | `nothing_left_to_try of string option * string ]
  with sexp

  val parse:
    ?debug:bool ->
    syntax_error:(location -> [> syntax_error] -> 'error) ->
    ('dsl, 'error) t -> Sexplib.Sexp.Annotated.t -> ('dsl, 'error) result

  val parse_string:
    ?debug:bool ->
    syntax_error:(location -> [> syntax_error | `sexp of exn ] -> 'error) ->
    ('dsl, 'error) t -> string -> ('dsl, 'error) result

end

(**
{3 Implementation Of The Module }

*)
module Meta_parser : META_PARSER = struct

  open Sexplib
  open Sexplib.Std
  open Printf
  let (|>) x f = f x
  module Option = struct
    let value ~default o =
      match o with
      | Some s -> s
      | None -> default
  end
  module String = StringLabels
  module List = ListLabels
  let failwithf fmt =
    ksprintf failwith fmt

  type location = { start: int * int; stop: int * int } with sexp

  let location
      ?from_line ?from_character ?from_position
      ?to_line ?to_character ?to_position () =
    let start =
      let optval ~default o = match o with None -> default | Some s -> s in
      let default = -1 in
      match from_position with
      | Some p  -> p
      | None -> (optval ~default from_line, optval ~default from_character)
    in
    let stop =
      let optval ~default o = match o with None -> default | Some s -> s in
      let default = -1 in
      match to_position with
      | Some p  -> p
      | None -> (optval ~default to_line, optval ~default to_character)
    in
    {start; stop}

(**
A “*lazy GADT*”:
*)
  type (_, 'error) grammar =
    | Keyword: string -> (unit, 'error) grammar
    | Left_and_continue:
        int * ('a, 'error) grammar Lazy.t * ('b, 'error) grammar Lazy.t -> ('a * 'b, 'error) grammar
    | Sequence: ('a, 'error) grammar Lazy.t -> ('a list, 'error) grammar
    | Try_in_order: ('a, 'error) grammar Lazy.t list * string option -> ('a, 'error) grammar
    | Integer: (int, 'error) grammar
    | Float: (float, 'error) grammar
    | String: (string, 'error) grammar
    | Apply: ('a, 'error) grammar Lazy.t * ('a -> loc:location -> ('b, 'error) result) -> ('b, 'error) grammar

  type ('dsl, 'error) t = ('dsl, 'error) grammar Lazy.t

(**
A function to display grammars, as they can be recursive, we must set
a `max_level`:
*)

  let rec to_string_aux: type a. int -> (a, _) grammar -> string  =
    fun level  gram ->
      if level < 0 then "..."
      else
        let to_string: type a. (a, _) grammar -> string =
          fun x -> to_string_aux (level - 1) x in
        match gram with
        | Keyword s -> sprintf "Kwd %s" s
        | Left_and_continue (number, lg, rg) ->
          sprintf "parse %d items [%s] and continue with [%s]" number
            (Lazy.force lg |> to_string) (Lazy.force rg |> to_string)
        | Sequence g -> sprintf "sequence [%s]" (Lazy.force g |> to_string)
        | Try_in_order (gl, err) ->
          sprintf "[%s]: try %s"
            (Option.value ~default:"NO-NAME" err)
            (List.map gl ~f:(fun g -> Lazy.force g |> to_string |> sprintf "[%s]")
             |> String.concat ~sep:" then ")
        | Integer -> "int"
        | Float -> "float"
        | String -> "string"
        | Apply (lg, f) -> sprintf "{%s}" (Lazy.force lg |> to_string)

  let to_string ?(max_level=3) t = (Lazy.force t |> to_string_aux max_level)


(**

The basic constructors:
*)

  let keyword s = lazy (Keyword s)
  let left_and_continue ?(number=1) left continue =
    lazy (Left_and_continue (number, left, continue))
  let sequence lg = lazy (Sequence (lg))
  let try_in_order ?name l = lazy (Try_in_order (l, name))

  let integer = lazy Integer
  (* let identifier = lazy Identifier; *)
  let string = lazy String
  let float = lazy Float

  let apply g ~f = lazy (Apply (g, f))

(**
The constructors [tagged] and [tuple] are examples of constructors
build from the lower-level ones:
*)
  let tagged lg ~tag =
    apply
      (left_and_continue (keyword tag) lg)
      (fun ((), c) ~loc -> return c)

  let tuple x y = left_and_continue x y


  (**
The functions [find_annotated_exn] and [remake_annotated_from_list_exn]
are useful for manipulating `Sexp.Annotated.t` values.
  *)
  let find_annotated_exn a t =
    match (Sexp.Annotated.find_sexp a t) with
    | Some s -> s
    | None ->
      failwithf "find_annotated_exn: %s In %s"
        (Sexp.to_string_hum t) (Sexp.Annotated.get_sexp a |> Sexp.to_string_hum) ()

  let remake_annotated_from_list_exn range annotated t =
    let open Sexp in
    (Annotated.List (range, List.map t ~f:(find_annotated_exn annotated), List t))


(**

And finally the parsing:
*)
  type syntax_error =
    [ `not_a_float of string
    | `incomplete_left_side of string * string
    | `not_an_integer of string
    | `no_matching_rule of string
    | `nothing_left_to_try of string option * string ]
  with sexp

  let parse ?(debug=false) ~syntax_error grammar sexp =
    let gram_to_string = to_string in
    let loc_of_range range =
      let open Sexp.Annotated in
      { start = range.start_pos.line, range.start_pos.col;
        stop = range.end_pos.line, range.end_pos.col;} in
    let parsing_error range e =
      fail (syntax_error (loc_of_range range) e) in
    let debug fmt =
      ksprintf (fun s -> if debug then eprintf "SPD: %s\n%!" s) fmt in
    let open Sexp in
    debug "go parse !";
    let rec go: type b. (b, 'e) grammar Lazy.t -> Sexp.Annotated.t -> (b, 'e) result =
      fun grammar annotated ->
        let sexp = Annotated.get_sexp annotated in
        let range = Annotated.get_range annotated in
        debug "%s Vs %s"
          (Sexp.to_string sexp) (to_string_aux 3 (Lazy.force grammar));
        match sexp, Lazy.force grammar with
        | any, Apply (gram, f) ->
          go gram annotated
          >>= fun r ->
          f ~loc:(loc_of_range range) r
        | Atom a, Keyword k when Atom k = Atom a ->
          (* say "Kwd: %s" a; return () *)
          return ()
        | List (l), Left_and_continue (number, gramleft, gramright) 
          when List.length l > number ->
          let rec split_at n acc l =
            match l, n with
            | l, 0 -> Some (List.rev acc, l)
            | h :: t, n -> split_at (n - 1) (h :: acc) t
            | [], _ -> None
          in
          debug "left_and_continue: number: %d" number;
          begin match split_at number [] l with
          | Some (left, right) ->
            debug "left_and_continue: left: %s" (Sexp.to_string_hum (List left));
            go gramleft (remake_annotated_from_list_exn range annotated left)
            >>= fun left ->
            debug "left_and_continue: right: %s" (Sexp.to_string_hum (List right));
            go gramright (remake_annotated_from_list_exn range annotated right)
            >>= fun right ->
            return (left, right)
          | None ->
              parsing_error range (`incomplete_left_side
                                     (Sexp.to_string_hum (List l),
                                      gram_to_string ~max_level:4 grammar))
          end
        | List l, Sequence subgram ->
          List.fold_left l ~init:(return []) ~f:(fun prev_m sexp ->
              prev_m >>= fun l ->
              go subgram (find_annotated_exn annotated sexp)
              >>= fun r ->
              return (l @ [r]))
        | any, Try_in_order (subgrams, error_name)  ->
          let rec loop = function
          | [] ->
            parsing_error range (`nothing_left_to_try
                                   (error_name,
                                    gram_to_string ~max_level:4 grammar))
          | h :: t ->
            match go h annotated with
            | `Ok o -> return o
            | `Error e -> loop t
          in
          loop subgrams
        | Atom a, String -> (* say "Str: %s" a;  *)return a
        | Atom a, Integer -> (* say "Int: %s" a; *)
          (try return (int_of_string a) with
           | e -> parsing_error range (`not_an_integer a))
        | Atom a, Float -> (* say "Int: %s" a; *)
          (try return (float_of_string a) with
           | e -> parsing_error range (`not_a_float a))
        | List [one], gram -> go (lazy gram) (find_annotated_exn annotated one)
        | any, _ ->
          parsing_error range (`no_matching_rule (Sexp.to_string any))
    in
    go grammar sexp

  let parse_string ?(debug=false) ~syntax_error grammar sexp =
    (
      let open Pre_sexp in
      try `Ok (Sexp.Annotated.of_string sexp)
      with
      | Parse_error {parse_state; _} as e ->
        let pos =
          match parse_state with
          | `Annot s -> s.parse_pos
          | `Sexp s -> s.parse_pos
        in
        let location =
          location ()
            ~from_line:pos.Parse_pos.text_line
            ~from_character:pos.Parse_pos.text_char
            ~to_line:pos.Parse_pos.text_line
            ~to_character:pos.Parse_pos.text_char in
        `Error (syntax_error location (`sexp e))
      | e -> `Error (syntax_error (location ()) (`sexp e)))
    >>= fun sexp ->
    parse ~debug ~syntax_error grammar sexp

end
